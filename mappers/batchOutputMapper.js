'use strict';

const PAINT_TYPES = require('../enums/paintTypes');

module.exports = {
  getOutputString
};

function getOutputString(batch) {
  const resultArray = [];

  Object.keys(batch).forEach((key) => {
    const type = batch[key].type ? batch[key].type : PAINT_TYPES.GLOSSY;
    resultArray.push(type === PAINT_TYPES.MATTE ? 1 : 0);
  });

  return resultArray.join(' ');
}
