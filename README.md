# Overview #

This is a node.js solution to the paint factory coding test.

### Execution ###

The project has a data file in the root directory containing the sample data provided in the assessment. The default app behaviour extracts this data but you can override the path with the environment variable: _inputFilePath_

Install packages:
```sh
$ npm install
```

Run with provided sample data: (data file in root dir)
```sh
$ npm start
```

Run with custom input file: (or overwrite data file in root dir)
```sh
$ inputFilePath='/path/to/file' npm start
```

### Tests ###

To run unit tests and calculate coverage:
  
```sh
$ npm test
```

To run integration test with sample data:

```sh
$ npm run integration
```

### Memory Consideration ###
This implementation will be memory greedy as it reads the entire file contents into memory. If this becomes a performance problem an alternative approach would be to read and process each test case one at a time.

In production a solution would be to split the implementation into a Reader and Worker design. The reader app can extract the cases, store and push to a message queue. Worker apps can pull from the queue as needed and can be effectively scaled horizontally based on workload.