'use strict';

const batchOutputMapper = require('../mappers/batchOutputMapper');
const fs = require('fs');
const parsingUtil = require('../utils/parsingUtil');
const readline = require('readline');
const batchService = require('../services/batchService');

module.exports = {
  launch
};

function launch(cb) {
  const parsedLines = [];

  readline
    .createInterface({
      input: fs.createReadStream(process.env.inputFilePath || 'data')
    })
    .on('line', (line) => {
      parsedLines.push(line.trim());
    })
    .on('close', () => {
      const testCases = parsingUtil.extractTestCases(parsedLines);
      testCases.forEach((a, i) => {
        const result = batchService.processTestCase(a);
        const resultString = result ? batchOutputMapper.getOutputString(result) : 'IMPOSSIBLE';
        /* eslint no-console: 0 */
        console.log(`Case #${i + 1}: ${resultString}`);
      });

      if (cb) {
        cb();
      }
    });
}
