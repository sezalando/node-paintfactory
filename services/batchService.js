'use strict';

const Batch = require('../models/batch');
const batchUtil = require('../utils/batchProcessingUtil');

module.exports = {
  processTestCase
};

function processTestCase(testCase) {
  const PAINT_BATCH = new Batch(testCase.totalColors);
  return _process(PAINT_BATCH, testCase.customers);
}

function _process(PAINT_BATCH, customers) {
  const queue = batchUtil.getUnresolvedCustomers(customers, PAINT_BATCH);

  if (queue.length === 0) {
    return PAINT_BATCH;
  }

  for (let i = 0; i < queue.length; i++) {
    const customer = queue[i];
    const validPref = batchUtil.getAvailablePaintPref(PAINT_BATCH, customer.paintPreferences);

    if (!validPref) {
      return null;
    }

    PAINT_BATCH[validPref.id].type = validPref.type;
    PAINT_BATCH[validPref.id].restricted = validPref.restrict;
  }

  return _process(PAINT_BATCH, customers);
}
