'use strict';

/* eslint import/no-extraneous-dependencies: 0 */
const launcher = require('../delegates/launchDelegate');
const expect = require('chai').expect;
const sinon = require('sinon');
const expectedResults = [
  'Case #1: 1 0 0 0 0',
  'Case #2: IMPOSSIBLE'
];

describe('paintfactory-integration', () => {
  before(() => {
    sinon.spy(console, 'log');
  });

  after(() => {
    console.log.restore;
  });

  it('should output expected results', (done) => {
    process.env.inputFilePath = './test-integration/data/data';
    /* eslint global-require: 0 */
    launcher.launch(() => {
      expectedResults.forEach((r) => {
        /* eslint no-console: 0 */
        expect(console.log.calledWith(r)).to.be.true;
      });

      done();
    });
  });
});
