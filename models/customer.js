'use strict';

module.exports = class Customer {
  constructor() {
    this.customerId = null;
    this.paintPreferences = [];
  }
};
