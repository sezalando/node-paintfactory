'use strict';

module.exports = class PaintPreference {
  constructor(id) {
    this.id = id;
    this.type = null;
  }
};
