'use strict';

module.exports = class {
  constructor(totalColors) {
    let count = 1;

    while (count <= totalColors) {
      this[count] = {
        type: null,
        restricted: false
      };
      count++;
    }
  }
};
