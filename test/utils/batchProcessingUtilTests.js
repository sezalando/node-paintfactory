'use strict';

const expect = require('chai').expect;
const batchUtil = require('../../utils/batchProcessingUtil');

describe('batchProcessingUtil', () => {
  let testBatch;
  let testPrefs;
  let testCustomers;

  beforeEach(() => {
    testBatch = {
      1: { type: 'GLOSSY', restricted: true },
      2: { type: 'MATTE', restricted: false },
      3: { type: null, restricted: false },
      4: { type: 'MATTE', restricted: true },
      5: { type: null, restricted: false }
    };
    testPrefs = [
      { id: 4, type: 'GLOSSY' },
      { id: 3, type: 'MATTE' }
    ];
    testCustomers = [
      { paintPreferences: [{ id: 4, type: 'GLOSSY' }] },
      { paintPreferences: [{ id: 1, type: 'GLOSSY' }] }
    ];
  });

  describe('getAvailablePaintPref', () => {
    it('should return pref when unset in batch', () => {
      expect(batchUtil.getAvailablePaintPref(testBatch, testPrefs)).to.deep.equal({
        id: 3,
        restrict: true,
        type: 'MATTE'
      });
    });

    it('should return pref when value already set in batch', () => {
      testBatch[4].type = 'GLOSSY';
      expect(batchUtil.getAvailablePaintPref(testBatch, testPrefs)).to.deep.equal({
        id: 4,
        restrict: false,
        type: 'GLOSSY'
      });
    });

    it('should return pref when batch value set but unrestricted', () => {
      testBatch[3].type = 'GLOSSY';
      expect(batchUtil.getAvailablePaintPref(testBatch, testPrefs)).to.deep.equal({
        id: 3,
        restrict: true,
        type: 'MATTE'
      });
    });

    it('should return null if no pref can be set', () => {
      testBatch[3].type = 'GLOSSY';
      testBatch[3].restricted = true;
      expect(batchUtil.getAvailablePaintPref(testBatch, testPrefs)).to.be.null;
    });
  });

  describe('getUnresolvedCustomers', () => {
    it('should return customers without preference set in batch', () => {
      const expectation = [testCustomers[0]];
      expect(batchUtil.getUnresolvedCustomers(testCustomers, testBatch)).to.deep.equal(expectation);
    });
  });
});
