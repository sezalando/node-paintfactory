'use strict';

const expect = require('chai').expect;
const parsingUtil = require('../../utils/parsingUtil');

describe('parsingUtil', () => {
  let testLines;

  beforeEach(() => {
    testLines = ['2', '5', '3', '1 1 1', '2 1 0 2 0', '1 5 0', '2', '1', '1 3 1'];
  });

  describe('extractTestCases', () => {
    it('should build expected test cases', () => {
      const expectation = [{
        totalColors: 5,
        customers: [
          {
            customerId: 1,
            paintPreferences: [
              { id: 1, type: 'MATTE' }
            ]
          },
          {
            customerId: 3,
            paintPreferences: [
              { id: 5, type: 'GLOSSY' }
            ]
          },
          {
            customerId: 2,
            paintPreferences: [
              { id: 1, type: 'GLOSSY' },
              { id: 2, type: 'GLOSSY' }
            ]
          }
        ]
      },
      {
        totalColors: 2,
        customers: [
          {
            customerId: 1,
            paintPreferences: [
              { id: 3, type: 'MATTE' }
            ]
          }
        ]
      }
      ];
      expect(parsingUtil.extractTestCases(testLines)).to.deep.equal(expectation);
    });
  });
});
