'use strict';

const expect = require('chai').expect;
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('launchDelegate', () => {
  let launchDelegate;
  let batchServiceMock;
  let readlineMock;
  let parsingUtilMock;
  let interfaceMock;
  let mapperMock;
  let fsMock;

  beforeEach(() => {
    fsMock = {
      createReadStream: sinon.stub().returns('someStream')
    };
    interfaceMock = {
      on: sinon.stub().returnsThis()
    };
    readlineMock = {
      createInterface: sinon.stub().returns(interfaceMock)
    };
    batchServiceMock = { processTestCase: sinon.stub() };
    parsingUtilMock = { extractTestCases: sinon.stub().returns([]) };
    mapperMock = { getOutputString: sinon.stub() };

    launchDelegate = proxyquire('../../delegates/launchDelegate', {
      fs: fsMock,
      '../mappers/batchOutputMapper': mapperMock,
      '../utils/parsingUtil': parsingUtilMock,
      readline: readlineMock,
      '../services/batchService': batchServiceMock
    });
  });

  describe('launch', () => {
    it('should create readline interface with file stream', () => {
      launchDelegate.launch();
      expect(readlineMock.createInterface.calledWith({ input: 'someStream' })).to.be.true;
    });

    it('should create read stream with env var', () => {
      process.env.inputFilePath = 'some/path';
      launchDelegate.launch();
      expect(fsMock.createReadStream.calledWith('some/path')).to.be.true;
    });

    it('should parse lines on close', (done) => {
      launchDelegate.launch();
      const addLineCb = interfaceMock.on.firstCall.args[1];
      addLineCb('1');
      addLineCb('2');
      interfaceMock.on.secondCall.args[1]();
      expect(parsingUtilMock.extractTestCases.calledWith(['1', '2'])).to.be.true;
      done();
    });

    it('should process parsed test cases on close', (done) => {
      const mockCases = ['case1', 'case2'];
      parsingUtilMock.extractTestCases.returns(mockCases);
      launchDelegate.launch();
      interfaceMock.on.secondCall.args[1]();
      mockCases.forEach((c) => {
        expect(batchServiceMock.processTestCase.calledWith(c)).to.be.true;
      });
      done();
    });

    it('should map output string if test case returned', (done) => {
      const mockCases = ['case1', 'case2'];
      parsingUtilMock.extractTestCases.returns(mockCases);
      batchServiceMock.processTestCase.onFirstCall().returns(null);
      batchServiceMock.processTestCase.onSecondCall().returns(mockCases[1]);
      launchDelegate.launch();
      interfaceMock.on.secondCall.args[1]();
      expect(mapperMock.getOutputString.calledOnce).to.be.true;
      expect(mapperMock.getOutputString.calledWith('case2')).to.be.true;
      done();
    });

    it('should call optional cb on completion', (done) => {
      const cb = sinon.stub();
      launchDelegate.launch(cb);
      interfaceMock.on.secondCall.args[1]();
      expect(cb.called).to.be.true;
      done();
    });
  });
});
