'use strict';

const expect = require('chai').expect;
const mapper = require('../../mappers/batchOutputMapper');

describe('outputMapper', () => {
  let testBatch;

  beforeEach(() => {
    testBatch = {
      1: { type: 'GLOSSY', restricted: true },
      2: { type: 'MATTE', restricted: false },
      3: { type: null, restricted: false },
      4: { type: 'GLOSSY', restricted: true },
      5: { type: null, restricted: false }
    };
  });

  describe('getOutputString', () => {
    it('should return expected output string', () => {
      const expectation = '0 1 0 0 0';
      expect(mapper.getOutputString(testBatch)).to.equal(expectation);
    });
  });
});
