'use strict';

const expect = require('chai').expect;
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('batchService', () => {
  let batchService;
  let mockTestCase;
  let batchUtilMock;
  let baseBatchMock;

  beforeEach(() => {
    baseBatchMock = {
      1: { type: null, restricted: false },
      2: { type: null, restricted: false },
      3: { type: null, restricted: false },
      4: { type: null, restricted: false },
      5: { type: null, restricted: false }
    };
    batchUtilMock = {
      getUnresolvedCustomers: sinon.stub(),
      getAvailablePaintPref: sinon.stub().returns({ id: 5, type: null, restrict: false })
    };
    batchUtilMock.getUnresolvedCustomers.onFirstCall().returnsArg(0);
    batchUtilMock.getUnresolvedCustomers.onSecondCall().returns([]);
    mockTestCase = {
      totalColors: 5,
      customers: [{
        customerId: 1,
        paintPreferences: [
          { id: 1, type: 'MATTE' }
        ]
      },
      {
        customerId: 2,
        paintPreferences: [
          { id: 1, type: 'GLOSSY' },
          { id: 2, type: 'GLOSSY' }
        ]
      },
      {
        customerId: 3,
        paintPreferences: [
          { id: 5, type: 'GLOSSY' }
        ]
      }
      ]
    };
    batchService = proxyquire('../../services/batchService', {
      '../utils/batchProcessingUtil': batchUtilMock
    });
  });

  describe('processTestCase', () => {
    it('should return batch model when all customers resolved', () => {
      expect(batchService.processTestCase(mockTestCase)).to.deep.equal(baseBatchMock);
    });

    it('should return null if customer preference conflict detected', () => {
      batchUtilMock.getAvailablePaintPref.returns(null);
      mockTestCase.customers[2].paintPreferences[0].id = 1;
      expect(batchService.processTestCase(mockTestCase)).to.be.null;
    });

    it('should extract customer preferences for each customer', () => {
      batchService.processTestCase(mockTestCase);
      mockTestCase.customers.forEach((c) => {
        expect(batchUtilMock.getAvailablePaintPref.calledWith(baseBatchMock, c.paintPreferences)).to.be.true;
      });
    });
  });
});
