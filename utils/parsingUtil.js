'use strict';

const Customer = require('../models/customer');
const PaintPreference = require('../models/paintPreference');
const PAINT_TYPES = require('../enums/paintTypes');
const TestCase = require('../models/testCase');

module.exports = {
  extractTestCases
};

function extractTestCases(parsedLines) {
  const testCases = [];
  const testCount = parseInt(parsedLines[0]);
  parsedLines.shift();

  let parsedCount = 0;
  while (parsedCount < testCount) {
    testCases.push(extractTestCase(parsedLines));
    parsedCount++;
  }

  return testCases;
}

function extractTestCase(lines) {
  const testCase = new TestCase();
  testCase.totalColors = parseInt(lines.shift());

  const customerCount = parseInt(lines.shift());
  let parsedCount = 0;
  while (parsedCount < customerCount) {
    const customer = parseCustomerModel(lines.shift());
    customer.customerId = parsedCount + 1;
    testCase.customers.push(customer);
    parsedCount++;
  }

  testCase.customers.sort((x, y) => x.paintPreferences.length > y.paintPreferences.length);

  return testCase;
}

function parseCustomerModel(line) {
  const lineSplit = line.split(' ');
  lineSplit.shift();
  const customer = new Customer();

  lineSplit.forEach((val, i) => {
    if (i % 2 !== 0) {
      const paintType = (parseInt(val) === 0) ? PAINT_TYPES.GLOSSY : PAINT_TYPES.MATTE;
      customer.paintPreferences[customer.paintPreferences.length - 1].type = paintType;
    } else {
      const id = parseInt(lineSplit[i]);
      customer.paintPreferences.push(new PaintPreference(id));
    }
  });

  // Push matte preferences to the end
  customer.paintPreferences.sort((x) => x.type === PAINT_TYPES.MATTE);

  return customer;
}
