'use strict';

module.exports = {
  getAvailablePaintPref,
  getUnresolvedCustomers
};

function getAvailablePaintPref(batch, prefs) {
  for (let i = 0; i < prefs.length; i++) {
    const pref = prefs[i];
    /*
     Set preference if:
       1: Preferred type is unset
       2: Preferred type is already set
       3: Preferred type is not restricted as a final preference
    */
    const prefAvailable = !batch[pref.id].type || batch[pref.id].type === pref.type;
    if (prefAvailable || !batch[pref.id].restricted) {
      return {
        id: pref.id,
        restrict: (prefs.length - 1) === i,
        type: pref.type
      };
    }
  }

  return null;
}

function getUnresolvedCustomers(customers, PAINT_BATCH) {
  const returnItems = [];
  customers.forEach((c) => {
    if (!_isCustomerSatisified(c, PAINT_BATCH)) {
      returnItems.push(c);
    }
  });

  return returnItems;
}

function _isCustomerSatisified(customer, batch) {
  for (let i = 0; i < customer.paintPreferences.length; i++) {
    const pref = customer.paintPreferences[i];
    if (batch[pref.id].type === pref.type) {
      return true;
    }
  }

  return false;
}
